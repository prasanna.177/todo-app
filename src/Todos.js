import React from "react";
import Todo from "./Todo";
import ErrorBoundary from "./ErrorBoundary";
import { addTodo } from "./actions";
import { reducer, initialState } from "./reducer";
const { Form, FormControl, Button } = require("react-bootstrap");

const Todos = () => {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  const [text, setText] = React.useState("");
  const onChange = (evt) => {
    setText(evt.target.value);
  };

  const onAddClick = () => {
    dispatch(addTodo({ text: text, complete: false }));
  };

  // input
  // button
  return (
    <>
      <Form>
        <FormControl
          type="text"
          name="username"
          placeholder="enter todo"
          defaultValue=""
          value={text}
          onChange={onChange}
        />
        <Button variant="info" onClick={onAddClick}>
          Add
        </Button>
      </Form>
      <ul>
        {state.todos.map((todo) => {
          return (
            <ErrorBoundary>
              <li>
                <Todo dispatch={dispatch} item={todo} />
              </li>
            </ErrorBoundary>
          );
        })}
      </ul>
    </>
  );
};

export default Todos;
