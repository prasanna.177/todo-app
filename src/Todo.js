import { Form, Button } from "react-bootstrap";
import { completeTodo, deleteTodo } from "./actions";

const Todo = ({ dispatch, item }) => {
  const onChange = (_evt) => {
    dispatch(completeTodo(item.id, !item.complete));
  };

  const onDeleteClicked = () => {
    dispatch(deleteTodo(item.id));
  };
  console.log({ item });
  return (
    <div style={{ display: "flex" }}>
      <Form.Check
        checked={item.complete}
        onChange={onChange}
        label={item.text}
      />
      <Button variant="danger" onClick={onDeleteClicked}>
        Delete
      </Button>
    </div>
  );
};

export default Todo;
