export const reducer = (state, action) => {
  console.log({ state, action });
  switch (action.type) {
    case "DELETE_TODO": {
      const { todos } = state;
      const newTodos = todos.filter((todo) => {
        return todo.id !== action.id;
      });
      return {
        ...state,
        todos: newTodos,
      };
    }
    case "ADD_TODO": {
      const { item } = action;
      const { todos } = state;
      let newTodoIndex = state.id + 1;
      let newTodo = {
        ...item,
        id: newTodoIndex,
      };
      const newTodos = [...todos, newTodo];

      return {
        ...state,
        id: newTodoIndex,
        todos: newTodos,
      };
    }
    case "COMPLETE_TODO": {
      const { id, complete } = action;
      const { todos } = state;
      const newTodos = todos.map((todo) => {
        if (todo.id === id) {
          return {
            ...todo,
            complete: complete,
          };
        } else {
          return {
            ...todo,
          };
        }
      });
      console.log({ newTodos });
      return {
        ...state,
        todos: newTodos,
      };
    }
    default: {
      return state;
    }
  }
};

export const initialState = {
  todos: [],
  id: 0,
};
