export const deleteTodo = (id) => {
  return {
    type: "DELETE_TODO",
    id,
  };
};

export const completeTodo = (id, complete) => {
  return {
    type: "COMPLETE_TODO",
    id,
    complete,
  };
};

export const addTodo = (item) => {
  return {
    type: "ADD_TODO",
    item,
  };
};
